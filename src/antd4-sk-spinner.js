
import { AntdSkSpinner } from '../../sk-spinner-antd/src/antd-sk-spinner.js';

export class Antd4SkSpinner extends AntdSkSpinner {

    get prefix() {
        return 'antd4';
    }

}
